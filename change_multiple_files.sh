#!/bin/bash

while read -r FILENAME ; do
    GWID=${FILENAME%%_*}
    FILETIME=${FILENAME#*_}
    # echo ${FILETIME}
    FILEYEAR=${FILETIME%%_*}
    # echo ${FILEYEAR}
    FILEMONTH=${FILETIME#*_}
    # echo ${FILEMONTH}
    FILEMONTH=${FILEMONTH%_*}
    # echo ${FILEMONTH}
    FILEDATE=${FILENAME##*_}
    # echo ${FILEDATE}
    echo "Applying metadata to file ${FILENAME}: ${FILEYEAR}-${FILEMONTH}-${FILEDATE} ${GWID}"

    sed -i -e "s/^/${FILEYEAR}-${FILEMONTH}-${FILEDATE}T/" toLogstash_20200614/${FILENAME}
    sed -i -e "s/ - / ${GWID} /" /toLogstash_20200614/${FILENAME}
    # sed -i -e "s/ remark ${GWID} / remark - /" toLogstash_20200614/${FILENAME}
done < <(ls -A toLogstash_20200614/)