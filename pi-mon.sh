!/bin/bash
# Temperature of Raspberry Pi 2/3 for ARM CPU and GPU
# -------------------------------------------------------
cputemp=$(</sys/class/thermal/thermal_zone0/temp)
echo "$(date) @ $(hostname)"
echo "-------------------------------------------"
echo "GPU => $(/opt/vc/bin/vcgencmd measure_temp)"
echo "CPU => $((cputemp/1000))'C"
