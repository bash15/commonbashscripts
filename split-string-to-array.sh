#!/bin/bash

TIMEUPLOAD="07:55:49.150"

echo "TIMEUPLOAD: ${TIMEUPLOAD}"

OIFS=$IFS
IFS=':'
declare -a TIMEUPLOADS=(${TIMEUPLOAD})
IFS=$OIFS
echo "TIMEUPLOADS[1]: ${TIMEUPLOADS[1]}"

j=1
FILEHOUR=05
FILEMIN=$((${j}*5))
echo -e "\nFILE Time: ${FILEHOUR}:${FILEMIN}"
echo -e "TIMEUPLOAD: ${TIMEUPLOAD}\n"
if [[ ${FILEHOUR} -eq ${TIMEUPLOADS[0]} ]]; then
    MINDIFF=$((${TIMEUPLOADS[1]} - ${FILEMIN}))
    echo "MINDIFF: ${MINDIFF}"
    if [[ ${MINDIFF} -gt 10 ]]; then
        echo "${STRTOGREP1} was uploaded at ${TIMEUPLOAD}, late > 10 mins "
    elif [[ ${MINDIFF} -gt 5 ]]; then
        echo "${STRTOGREP1} was uploaded at ${TIMEUPLOAD}, late > 5 mins "
    fi
else
    echo -e "\nOver different hours"
    HOURDIFF=$((${TIMEUPLOADS[0]} - ${FILEHOUR}))
    MINDIFF=$((60*${HOURDIFF} - ${FILEMIN} + ${TIMEUPLOADS[1]}))
    echo "HOURDIFF: ${HOURDIFF}"
    echo "MINDIFF: ${MINDIFF}"
fi

# for x in ${SAMPLE}
# do
#     echo "${x}"
# done
