#!/bin/bash
#elm_check_delay_n_missing_files.sh

STARTTIME=$(date '+%s')

DEBUG=0
CREATEDDATE="12 May 2020"
UPDATEDDDATE="17 May 2020"
VERSION="0.1.2"
FOLDER=""
FILEYEAR=""
FILEMONTH=""
FILEDATE=""
FILENEXTDATE="03"
FILEHOUR="00"
FILEMINGAP=5 # The file is created with block 5 mins gap
LOGFILE=""
LOGFILENEXTDAY=""
ICNT=0
ICNTDELAY=0
ICNTDELAYTOTAL=0
ICNTUPLOADEDFILESTOTAL=0
ICNTMISSINGFILES=288
VERBOSE=0
DELAYMINTH=5 # Min threshold to consider the file uploaded late

debugPrint() {
  if [ ${DEBUG} -eq 1 ]; then
    echo -e "${1}"
  fi
}

showHelp() {
  echo -e "Version: ${VERSION}, created by John Nguyen on ${CREATEDDATE}, last update on ${UPDATEDDDATE}\n"
  # `cat << EOF` This means that cat should stop reading when EOF is detected
  cat << EOF
Automatically analyze diagnostic log file.
On Windows, can run by git bash.
For now, only analyze delay and missing files, will add more features in the future

Usage: elm_check_delay_n_missing_files.sh [OPTIONS]

  -d, --date    <date>      Date to be analyzed
                            - If no input, Today will be considered for analyzation

  -D, --debug               Print all debug messages

  -f, --file    <files>     Input the path to diagnostic log file
                            - If no input, file to be analyzed will base on Today

  -F, --folder  <folder>    Input the path to folder contains diagnostic log files
                            - Log files will be auto determined by the inputed date

  -h, --help                Display this help

  -m, --month   <month>     Month to be analyzed
                            - If no input, This month will be considered for analyzation

  -v, -version,             Version credit info

  -y, --year    <year>      Year to be analyzed
                            - If no input, This year will be considered for analyzation

  -V, --verbose             Run script in verbose mode. Will print messages each step of execution.
                            - Set xtrace and verbose mode by command "set -xv"

Example:
  elm_check_delay_n_missing_files.sh -d 13 -f FP2-SL3/diagnosticLog/b827eb7046e1_2020_05_13
# or
  elm_check_delay_n_missing_files.sh -d 13 -f "FP2-SL3/diagnosticLog/b827eb7046e1_2020_05_13 FP2-SL3/diagnosticLog/b827eb7046e1_2020_05_14"
# or
  elm_check_delay_n_missing_files.sh -d 13 -F FP2-SL3/diagnosticLog/
EOF
  # EOF is found above and hence cat command stops reading. This is equivalent to echo but much neater when printing out.
}

showVersion() {
  echo -e "Version: ${VERSION}, created on by John Nguyen ${CREATEDDATE}, last update on ${UPDATEDDDATE}\n"
}

# Parsing arguments from console
OPTIONS=$(getopt -l date:,debug,file:,folder:,help,month:,version,year:,verbose -- d:Df:F:hm:vy:V "$@") || exit 1
eval set -- "$OPTIONS"
while true; do
  case "$1" in
  -d|--date) FILEDATE=${2}; shift 2;;
  -D|--debug) DEBUG=1; shift;;
  -f|--file) LOGFILE=${2}; shift 2;;
  -F|--folder) FOLDER=${2}; shift 2;;
  -h|--help) showHelp; exit 0;;
  -m|--month) FILEMONTH=${2}; shift 2;;
  -v|--version) showVersion; exit 0;;
  -y|--year) FILEYEAR=${2}; shift 2;;
  -V|--verbose) VERBOSE=1; set -xv; shift;;
  --) shift; break;;
  *) echo Error; exit 1;;
  esac
done

if [ -z "${FILEDATE}" ]; then
  FILEDATE=$(date +'%d')
fi

if [ -z "${FOLDER}" ]; then
  FOLDER="./*/"
fi
echo "Check in folder: ${FOLDER}"

if [ -z "${FILEMONTH}" ]; then
  FILEMONTH=$(date +'%m')
fi

if [ -z "${FILEYEAR}" ]; then
  FILEYEAR=$(date +'%Y')
fi

if [ -z "${LOGFILE}" ]; then
  LOGFILE="*_${FILEYEAR}_${FILEMONTH}_${FILEDATE}"
  LOGFILE="${FOLDER}${LOGFILE}"
  # Check if FILEDATE is the last day of month
  LASTDAYOFMONTH=$(cal $(date +"${FILEMONTH} %Y") | awk 'NF {DAYS = $NF}; END {print DAYS}')
  if [ ${FILEDATE} -eq ${LASTDAYOFMONTH} ]; then
    if [ ${FILEMONTH} -lt 10 ]; then
      LOGFILENEXTDAY="*_${FILEYEAR}_0$((10#${FILEMONTH} + 1))_01"
    else
      LOGFILENEXTDAY="*_${FILEYEAR}_$((10#${FILEMONTH} + 1))_01"
    fi
  else
    if [ ${FILEDATE} -lt 9 ]; then
      FILENEXTDATE="0$((10#${FILEDATE} + 1))"
    else
      ((FILENEXTDATE=10#${FILEDATE} + 1))
    fi
    LOGFILENEXTDAY="*_${FILEYEAR}_${FILEMONTH}_${FILENEXTDATE}"
  fi
  LOGFILENEXTDAY="${FOLDER}${LOGFILENEXTDAY}"
fi

echo "Check data files for the date: ${FILEYEAR}-${FILEMONTH}-${FILEDATE}"

echo -e "Processing files: ${LOGFILE} ${LOGFILENEXTDAY}"
if [ ! -f ${LOGFILE} ] || [ ! -f ${LOGFILENEXTDAY} ]; then
  echo "Either or both checking files do not exist! Exit"
  exit 0
fi

if [ -f "/drives/c/Program Files (x86)/Notepad++/n++.exe" ]; then
  # Path run on Mobaxterm
  /drives/c/Program\ Files\ \(x86\)/Notepad++/n++.exe ${LOGFILE} ${LOGFILENEXTDAY}
elif [ -f c:/'Program Files (x86)'/Notepad++/n++.exe ]; then
  # Path run on gitbash
  c:/'Program Files (x86)'/Notepad++/n++.exe ${LOGFILE} ${LOGFILENEXTDAY}
else
  echo "Cannot find Notepad++ to open log files, you can install Notepadd++ or open log files yourself."
fi

# This function will be run in background
# egrepper <String to grep> <File date> <Delay minutes threshold>
egrepper() {
  STRTOGREPTMP=${1}
  TMPFILE=${1}
  FILEDATE=${2}
  DELAYMINTH=${3}
  FILECNT=0
  ICNTDELAY=0

  echo > ${TMPFILE} # Create new file

  while read -r line ; do
    ((FILECNT++))
    DATEUPLOAD=${line%%:*} # Remove all character follow pattern from the end
    DATEUPLOAD=${DATEUPLOAD##*_}

    line=${line#*:}
    TIMEUPLOAD=${line%%.*} # Remove all character follow pattern from the end
    if [ ! -z ${TIMEUPLOAD} ]; then # Check if not empty, "-n" comparision must be done with inintialized var
      # Split the TIMEUPLOAD with delimiter is ":"
      OIFS=$IFS
      IFS=':'
      # declare -a TIMEUPLOADS=(${TIMEUPLOAD}) # Assign it to an array
      TIMEUPLOADS=(${TIMEUPLOAD}) # Assign it to an array
      IFS=$OIFS
    fi

    # avoid preceding the number by 0 if it's smaller than 10
    # debugPrint "TIMEUPLOADS: ${TIMEUPLOADS[*]}"
    if [ ${TIMEUPLOADS[0]} -lt 10 ]; then
      if [ ${TIMEUPLOADS[0]} -eq 00 ]; then
        HOURUPLOAD=0
      else
        HOURUPLOAD=${TIMEUPLOADS[0]//0}
      fi
    else
      HOURUPLOAD=${TIMEUPLOADS[0]}
    fi
    if [ ${TIMEUPLOADS[1]} -lt 10 ]; then
      if [ ${TIMEUPLOADS[1]} -eq 00 ]; then
        MINUPLOAD=0
      else
        MINUPLOAD=${TIMEUPLOADS[1]//0}
      fi
    else
      MINUPLOAD=${TIMEUPLOADS[1]}
    fi

    RAWFILE=$(echo ${line} | awk '{print $5}' )
    RAWFILE=${RAWFILE/gz./gz}

    TMP=${RAWFILE#*_*_*_} # Remove all character follow pattern
    TMP=${TMP/.gz} # Remove all sub string with .gz

    FILEMIN=${TMP#*_} # Remove all character follow pattern *_
    if [ ${FILEMIN} -lt 10 ]; then
      if [ ${FILEMIN} -eq 00 ]; then
        FILEMIN=0
      else
        FILEMIN=${FILEMIN/0}
      fi
    else
      FILEMIN=${FILEMIN}
    fi
    FILEMIN=$((${FILEMIN} * ${FILEMINGAP}))

    FILEHOURSTR=${TMP%_*} # Remove all character follow pattern from the end
    i=$((${#FILEHOURSTR} - 2)) # Get index of next last character
    FILEHOURSTR=${FILEHOURSTR:$i:2} # Get last 2 character
    if [ ${FILEHOURSTR} -lt 10 ]; then
      if [ ${FILEHOURSTR} -eq 00 ]; then
        FILEHOUR=0
      else
        FILEHOUR=${FILEHOURSTR/0}
      fi
    else
      FILEHOUR=${FILEHOURSTR}
    fi

    # Calculate the different bw uploading time and creating time of the file
    # debugPrint "FILEDATE: ${FILEDATE}, DATEUPLOAD: ${DATEUPLOAD}"
    if [ ${FILEDATE} -ne ${DATEUPLOAD} ]; then
      HOURUPLOAD=$((${HOURUPLOAD} + 24)) # File uploaded in the next day
    fi
    if [ ${FILEHOUR} -eq ${HOURUPLOAD} ]; then
      MINDIFF=$((${MINUPLOAD} - ${FILEMIN}))
      # debugPrint "MINDIFF: ${MINDIFF}"
    else
      # debugPrint "\nOver different hours"
      HOURDIFF=$((${HOURUPLOAD} - ${FILEHOUR}))
      # debugPrint "MINUPLOAD: ${MINUPLOAD}"
      MINDIFF=$((60*${HOURDIFF} - ${FILEMIN} + ${MINUPLOAD}))
      # debugPrint "HOURDIFF: ${HOURDIFF}"
      # debugPrint "MINDIFF: ${MINDIFF}"
    fi

    # debugPrint "\nRAWFILE: ${RAWFILE}, FILEHOUR: ${FILEHOUR}, FILEMIN: ${FILEMIN}"
    # debugPrint "TIMEUPLOAD: ${TIMEUPLOAD}, ${TIMEUPLOADS[0]} ${TIMEUPLOADS[1]}"
    # Compare to get delay time
    MINDIFF=$((${MINDIFF} - ${FILEMINGAP}))
    if [ ${MINDIFF} -gt 60 ]; then
      HOURDIFF=$((${MINDIFF}/60))
      MINDIFF=$((${MINDIFF} - ${HOURDIFF}*60))
      echo "${RAWFILE} was uploaded at ${TIMEUPLOAD} ${DATEUPLOAD}/${FILEMONTH}, late ${HOURDIFF} hour ${MINDIFF} minutes" >> ${TMPFILE}
      ((ICNTDELAY++))
    elif [ ${MINDIFF} -gt ${DELAYMINTH} ]; then
      echo "${RAWFILE} was uploaded at ${TIMEUPLOAD} ${DATEUPLOAD}/${FILEMONTH}, late ${MINDIFF} minutes" >> ${TMPFILE}
      ((ICNTDELAY++))
    fi
  done < <(egrep -IaH "Successfully.*.${STRTOGREPTMP}" ${LOGFILE} ${LOGFILENEXTDAY})

  # Append to the head of file
  # The first line already have break when we create by echo cmd, so don't need to add break line here
  echo -e "${FILECNT} files uploaded, ${ICNTDELAY} files delay$(cat ${TMPFILE})" > ${TMPFILE}
  echo -e "DONE\n$(cat ${TMPFILE})" > ${TMPFILE}
}

for i in {0..23}; do
  if [ ${i} -lt 10 ]; then
    FILEHOUR="0${i}"
  else
    FILEHOUR="${i}"
  fi
  STRTOGREP="${FILEYEAR}${FILEMONTH}${FILEDATE}${FILEHOUR}"
  egrepper ${STRTOGREP} ${FILEDATE} ${DELAYMINTH} & # Run in background
done;

for i in {0..23}; do
  if [ ${i} -lt 10 ]; then
    FILEHOUR="0${i}"
  else
    FILEHOUR="${i}"
  fi
  TMPFILE="${FILEYEAR}${FILEMONTH}${FILEDATE}${FILEHOUR}"

  # Keep checking if file process is done
  j=1
  SPINNER="/-\|" # Let user know to wait
  FILEDONE=""
  echo -n ' '
  while [ -z ${FILEDONE} ]; do
    FILEDONE="$(egrep DONE ${TMPFILE})"
    printf "\b${SPINNER:j++%${#SPINNER}:1}"
  done
  
  sed -i '1d' ${TMPFILE} # Remove the first line with DONE flag

  ICNTUPLOADEDFILES=$(head -n 1 ${TMPFILE} | awk '{print $1}')
  ICNTUPLOADEDFILESTOTAL=$((${ICNTUPLOADEDFILESTOTAL} + ${ICNTUPLOADEDFILES}))
  ICNTDELAY=$(head -n 1 ${TMPFILE} | awk '{print $4}')
  ICNTDELAYTOTAL=$((${ICNTDELAYTOTAL} + ${ICNTDELAY}))

  echo -e "\n${TMPFILE}: $(cat ${TMPFILE})"
  rm ${TMPFILE}
done

echo -e "\nTotal delay files: ${ICNTDELAYTOTAL}"
echo -e "Total missing files: $((${ICNTMISSINGFILES} - ${ICNTUPLOADEDFILESTOTAL}))"

STOPTIME=$(date '+%s')
DURATION=$((${STOPTIME} - ${STARTTIME}))
debugPrint "Duration: $(($((${DURATION} - ${DURATION}/86400*86400))%3600/60))m:$(($((${DURATION} - ${DURATION}/86400*86400))%60))s"