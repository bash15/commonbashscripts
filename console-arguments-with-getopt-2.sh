#!/bin/bash

# "--"": If no arguments follow this option, then the positional parameters are unset. Otherwise, the positional parameters 
# "::": Arguments must be next to the options,
#   ex:
#       ./test4.sh -c123456
#       ./test4.sh -c 123456 # cannot
OPTIONS=`getopt -l bb: -- ab:c::d: "$@"` || exit 1
eval set -- "$OPTIONS"
while true; do
        case "$1" in
        -a)     echo A; shift;;
        -b|--bb) echo "B:'$2'"; shift 2;;
        -c)     case "$2" in
                "")     echo C; shift 2;;
                *)      echo "C:'$2'"; shift 2;;
                esac;;
        -d)     case "$2" in
                "")     echo D; shift 2;;
                *)      echo "D:'$2'"; shift 2;;
                esac;;
        --)     shift; break;;
        *)      echo Error; exit 1;;
        esac
done